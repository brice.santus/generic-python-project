<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Generic Project](#generic-project)
    - [Getting Started](#getting-started)
        - [Setup Poetry](#setup-poetry)
        - [Pyenv (Optional)](#pyenv-optional)
        - [Install the dependencies](#install-the-dependencies)
    - [Running the code](#running-the-code)
        - [About the code](#about-the-code)
    - [Testing](#testing)
    - [Contributing](#contributing)
        - [Contributing to the code](#contributing-to-the-code)
    - [Maintain this documentation](#maintain-this-documentation)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Generic project

This project is a generic project skeleton.

## Getting Started

### Setup Poetry

This project uses [Poetry](https://python-poetry.org/docs/) as a package manager. It allows the use of virtual environments and the lock of package versions, whether they are in your dependencies or just sub-dependencies of these dependencies.

### Pyenv (Optional)

[Pyenv](https://github.com/pyenv/pyenv) is a Virtual env manager. You can setup pyenv if you want to manage easily your python version. It's not mandatory to work with Poetry

### Install the dependencies

Installing the dependencies is done by running the following command:

```
$ poetry install
```

## Running the code

For now, the only operation is a `Hello World`. In order to run it, you can do :

```
$ poetry run hello
```

### About the code

This cli was built using [click](https://click.palletsprojects.com/).

## Testing

This project uses [pytest](https://docs.pytest.org/en/latest/). You can run the test suite like so:

```
$ poetry run pytest
```

## Contributing

Contribution to this project can be done by opening issues on [the issue tracker](https://gitlab.com/fattybenji/generic-project/-/issues) or by pushing some code.

### Contributing to the code

There are some coding rules to respect. This project uses the following tools :

* [black](https://github.com/psf/black) is a code formatter
* [isort](https://github.com/timothycrosley/isort) is used to sort imports
* [pylint](https://github.com/PyCQA/pylint) is a standard python linter

In order to help developers respect those rules, [pre-commit](https://pre-commit.com/) has been set up. In order to enable it, after you install the dependencies, run the following command :

```
$ poetry run pre-commit install
```

### Commit messages

This project uses semantic versioning so it is important to follow the rules of [conventional commits](https://www.conventionalcommits.org/).

## Maintain this documentation

Use [doctoc](https://github.com/thlorenz/doctoc)

```bash
/# npm install -g doctoc
/# doctoc README.md --gitlab
```
